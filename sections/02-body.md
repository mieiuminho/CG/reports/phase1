# Generator

The point of the _Generator_ program is to generate the primitive figures
and write the vertices to a file that allows the engine to perform the
drawing of the object.

It's worth mentioning that we considered that our engine only understands
triangles. The previous statement means that in our `.3d` files there
will be sequences of 3 vertices that represent a triangle that is going to
be displayed by the _Engine_.

## Plane

Since the _Plane_ is only a square in the _**xz**_ plane which is centered
in the origin the strategy here was very simple. The ideia is to generate
the four vertices that define the _Plane_ and then build the 2 triangles.
The triangles are built respecting the right hand rule in order for the
plane to be pointing upwards.

![Plane divided in triangles](figures/plane.png){ height=170px}

In order to follow the right and rule we must write the vertices: 4, 1 and 2,
for the first triangle and 4, 2 and 3 for the second triangle (both sequences
in the presented order).

## Box

The _Box_ has 3 pairs of parallel faces. For each pair of faces we first
imagined the face centered in the origin and then moved it to the correct
place. Let's imagine, for example, the faces that are paralell to the
_**xy**_ axis: we first imagined the planes in the position defined by
_**z = 0**_ and then moved it to the front _z / 2_ units and then moved
it backwards _z_ units (in this case _z_ is the _z_ dimension of the box).
With these 2 steps we generate the planes that will make up for the box
faces.

![Box](figures/box_divisions.png){ height=270px }

The next step is to divide the faces in both vertically and horizontally
in the amount of divisions asked by the user. The strategy is simple: if
assume _n_ to be number of divisions asked by the user, in each face of
the box we'll have: $n * n$ faces with the same dimensions. So, th
strategy is to iterate through all of them and, consequently, write the
vertices in the `.3d` file.

## Sphere

We considered the _slices_ to be vertical segments in the sphere and the
_stacks_ to be horizontal segments as the following images shows. The
following image refers to the _slices_ as _sectors_.

![_Slices_ and _Stacks_ in the sphere](figures/sphere.png){ height=170px }

In order to divide the sphere in rectangles (that we will later on split in
triangles in order to draw the figure) we have to consider two angles: the
horizontal angle ($\theta$) that varies by going through the different
_slices_ and the vertical angle ($\phi$) that varies by going through the
different _stacks_.

![Sphere angles](figures/sphere_angles.png){ height=170px }

The main idea is to iterate through the stacks and the slices. In other
words, we iterate through the number of stacks and slices looking at
two at each time: the current one and the previous one. Let's have a
look at the following image:

![Rectangles in the spheres](figures/square_in_sphere.png){ height=150px }

Once we have the rectangle (meaning that we have the 4 points that define
the rectangle) the only thing left to do is to divide the rectangle into
two triangles.

We can divide each rectangle in triangles: the first triangle is formed by the
following vertices: $k_{1}$, $k_{1} + 1$ and $k_{2}$. The second triangle is
formed by the following vertices: $k_{1} + 1$, $k_{2}$ and $k_{2} + 1$.

## Cone

In this case the _stacks_ don't imply an angle to be iterated once the height
of a _stack_ is equal to the total height of the cone divided by the number
of _stacks_. As for the _slices_ we still need an angle to iterate through
all of them.

In each stack we have to picture a smaller cone in order to calculate it's
new base radius, as the image below shows:

![Cone's explanation](figures/cone_explanation.jpg){ height=250px }

Once we picture the smaller cone we have the 4 vertices that define a
rectangle: the 2 vertices that we get by iterating the slices of the
smaller cone's basis and the 2 vertices that we get by iterating the
slices of the bigger cone's basis.

# Engine

The engine is a very simple program with two major functions: parse the
lines of `.3d` files into an array of arrays that stores the vertices of
every triangle that will have to be drawn and draw the triangles whose
vertices were previously stored.

