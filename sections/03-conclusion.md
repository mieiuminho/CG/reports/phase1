# Conclusion {#sec:conclusion}

This phase of the project was absolutely crucial, once it made us realize
the work that goes into drawing some of the simples figures that appear on
our screens everyday. The work that we developed is going to be the basis
of the upcoming phases since we are going to build and develop on top of
this primitive figures.

