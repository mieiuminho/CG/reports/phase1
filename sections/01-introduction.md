# Introduction {#sec:introduction}

The goal of the assignement is to develop a mini scene graph based 3D engine
and provide usage examples that show its potential. The assignement is split
in four phases, each with a due date.

This report is related with 1st phase, it requires two applications: one to
generate files with the models information (in this phase only generate the
vertices for the model) and the engine itself which will read a configuration
file, written in XML, and display the models.

To create the model files an application (independent from the engine) will
receive as parameters the graphical primitive's type, other parameters
required for the model creation, and the destination file where the vertices
will be stored.

In this phase the following graphical primitives are required:

- Plane (a square in the _**xz**_ plane, centred in the origin, made with 2
  triangles)
- Box (requires _**x**_, _**y**_ and _**z**_ dimensions, and optionally the
  number of divisions)
- Sphere (requires radius, slices and stacks)
- Cone (requires bottom radius, height, slices and stacks)

\newpage

